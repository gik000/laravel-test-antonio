<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Middleware\VerifyCsrfToken;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContattiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');


Route::get('/welcome', [ContactController::class, 'welcome'])->name('contact.welcome');
Route::get('/form', [ContactController::class, 'contactForm'])->name('contact.form');
Route::post('/form/submit', [ContactController::class, 'contactFormSubmit'])->name('contact.submit');

