@extends('base')

@section('meta-title')
Contatto
@endsection

@section('content')
    <h1>Modulo di Contatto</h1>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{route('contact.submit')}}" method="post">
        @csrf
        <label for="nome">Nome:</label>
        <input type="text" id="nome" name="nome" required><br><br>
        
        <label for="cognome">Cognome:</label>
        <input type="text" id="cognome" name="cognome" required><br><br>
        
        <label for="email">Email:</label>
        <input type="text" id="email" name="email" required><br><br>
        
        <label for="telefono">Telefono:</label>
        <input type="tel" id="telefono" name="telefono" pattern="[0-9]{10}" required><br><br>
        
        <input type="checkbox" id="accettoPrivacy" name="accettoPrivacy" required>
        <label for="accettoPrivacy">Accetto l'<a href="privacy.html">Informativa sulla Privacy</a></label><br><br>
        
        <input type="submit" value="Invia">
    </form>
@endsection