<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    //
    public function welcome(){

        return 'ciao';
    }

    public function contactForm(){
        return view('form');
    }

    /**
     * Questa funzione effettua il submit del form
     *
    * @return \Illuminate\Http\Response
     */
    public function contactFormSubmit(Request $request){
        $data = $request->all();

        $validate = \Validator::make($data, [
            'nome'=> 'required|string',
            'email'=> 'required|email',
        ]);

        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        

        //loading view
        $view = view('contattami', $data);

        return response($view);
    }

}
